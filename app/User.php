<?php

namespace App;

use App\Models\District;
use App\Models\Facility;
use App\Models\FormData;
use App\Models\ProjectRoleForm;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract {
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected $table = 'users';

    public function cases() {
        return $this->hasMany(FormData::class, 'creator_id');
    }

    public function facilities() {
        return $this->belongsToMany(Facility::class, 'user_facility', 'user', 'facility');
    }

    public function districts() {
        return $this->belongsToMany(District::class, 'user_district', 'user', 'district');
    }

    public function forms() {
        //SELECT form_id FROM `project_role_forms` WHERE project = 'rabies' AND role = 1
        return ProjectRoleForm::where('role', $this->role)
            ->select('form_id')
            ->get()
            ->map(function($item){
                return $item->form_id;
            });
    }

    public  function getDistrictAsFacilityHack() {
        $c = $this->districts;

        $c = $c->map(function($item) {
            $item->pivot = 0;
            $item->district = $item->id;
            return $item;

        });

        return $c;
    }

}
