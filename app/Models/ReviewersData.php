<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReviewersData extends Model {

    protected $table = 'reviewers_data';
    protected $primaryKey = 'case_id';
    public $timestamps = false;

    public function case_() {
        return $this->belongsTo(FormData::class, 'case_id');
    }
}
