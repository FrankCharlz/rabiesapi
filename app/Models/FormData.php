<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class FormData extends Model {

    protected $table = 'form_data';
    protected $primaryKey = 'case_id';
    public $timestamps = false;

    public function review() {
        return $this->hasOne(ReviewersData::class, 'case_id');
    }

    public function creator() {
        return $this->belongsTo(User::class, 'creator_id');
    }

    public function notReviewed($hours) {
        return $this->belongsTo(User::class, 'creator_id');
    }



}
