<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{

    protected $table = 'region';
    protected $primaryKey = 'region_id';
    public $timestamps = false;

    public function country() {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function districts() {
        return $this->hasMany(District::class, 'region_id');
    }


}
