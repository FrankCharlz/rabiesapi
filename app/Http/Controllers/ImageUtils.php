<?php
/**
 * Created by PhpStorm.
 * User: Frank
 * Date: 20-Aug-17
 * Time: 23:09
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class ImageUtils {

    const UPLOADS_DESTINATION = "public/uploads";
    const THUMBNAIL_BACKGROUND = 'transparent';
    const THUMBNAIL_WIDTH = 100;

    public static function upload(Request $request, $post_field_name) {

        if ($request->hasFile($post_field_name) && $request->file($post_field_name)->isValid()) {

            try {
                $filename = $request->file($post_field_name)->getClientOriginalName();
                $request->file($post_field_name)->move(self::UPLOADS_DESTINATION, $filename);
                $path = self::UPLOADS_DESTINATION . '/' . $filename;

                self::makeThumbnail($path,
                    self::makeThumbnailPath($path),
                    self::THUMBNAIL_WIDTH,
                    self::THUMBNAIL_WIDTH,
                    self::THUMBNAIL_BACKGROUND
                );

                return $path;

            } catch (FileException $exception) {
                print_r("Could not move file ". $exception->getMessage());
                return null;
            }

        } else {
            return null;
        }

    }

    private static function makeThumbnail($filepath, $thumbpath, $thumbnail_width, $thumbnail_height, $background=false) {
        list($original_width, $original_height, $original_type) = getimagesize($filepath);
        if ($original_width > $original_height) {
            $new_width = $thumbnail_width;
            $new_height = intval($original_height * $new_width / $original_width);
        } else {
            $new_height = $thumbnail_height;
            $new_width = intval($original_width * $new_height / $original_height);
        }
        $dest_x = intval(($thumbnail_width - $new_width) / 2);
        $dest_y = intval(($thumbnail_height - $new_height) / 2);

        if ($original_type === 1) {
            $imgt = "ImageGIF";
            $imgcreatefrom = "ImageCreateFromGIF";
        } else if ($original_type === 2) {
            $imgt = "ImageJPEG";
            $imgcreatefrom = "ImageCreateFromJPEG";
        } else if ($original_type === 3) {
            $imgt = "ImagePNG";
            $imgcreatefrom = "ImageCreateFromPNG";
        } else {
            return false;
        }

        $old_image = $imgcreatefrom($filepath);
        $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height); // creates new image, but with a black background

        // figuring out the color for the background
        if(is_array($background) && count($background) === 3) {
            list($red, $green, $blue) = $background;
            $color = imagecolorallocate($new_image, $red, $green, $blue);
            imagefill($new_image, 0, 0, $color);
            // apply transparent background only if is a png image
        } else if($background === 'transparent' && $original_type === 3) {
            imagesavealpha($new_image, TRUE);
            $color = imagecolorallocatealpha($new_image, 0, 0, 0, 127);
            imagefill($new_image, 0, 0, $color);
        }

        imagecopyresampled($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
        $imgt($new_image, $thumbpath);
        return file_exists($thumbpath);
    }

    private static function makeThumbnailPath($path) {
        //adds thumb_ to the name of the image
        $arr = explode("/", $path);
        $arr[sizeof($arr) - 1] = "thumb_" . $arr[sizeof($arr) - 1];
        return implode('/', $arr);
    }

    private static function getUrlFromPath($path) {
        //path comes as public/uploads/filename.png
        //path must be uploads/filename.png

        $parts = explode('/', $path);
        $index = sizeof($parts) - 1;
        $filename = $parts[$index];
        return 'uploads/' . $filename;
    }

    public function renderImageCompat(Request $request) {

        $path = $request->get('path');

        $image_url = self::getUrlFromPath($path);

        $html = <<<EOT
        <html>
        <head>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
        </head>
        <body style='margin: 0;padding: 0;width: 100%;'>
        <img  src='$image_url' width='100%'>
        <body>
        </html>.
EOT;

        echo $html;
    }

    public function renderThumbCompat(Request $request) {

        $path = 'uploads' .  $request->get('path');
        $path = self::makeThumbnailPath($path); //thumbnail path

        $image_url = self::getUrlFromPath($path);

        header('Content-Type: image/png');
        header("Location: ".$image_url);
        exit();

    }
}
