<?php
/**
 * Created by PhpStorm.
 * User: Frank
 * Date: 18-Aug-17
 * Time: 15:44
 */

namespace App\Http\Controllers;

use App\Models\FormData;
use App\Models\ReviewersData;
use Illuminate\Support\Facades\DB;

class NotificationManager {

    public static function notifyOnCase($facilityId, $caseId) {
        //find reviewers for that facility and send them a notification

        $query_token = "
        SELECT fcm_token
        FROM users
        INNER JOIN user_facility ON(users.id=user_facility.user)
        WHERE role=2
        AND users.fcm_token IS NOT NULL
        AND user_facility.facility  = ?";


        $result = DB::select($query_token, [$facilityId]);
        $tokens = array_map(function($item) {return $item->fcm_token;}, $result);

        $data = array(
            'case_id' => $caseId,
            'category' => 'chw'
        );


        self::sendPushNotification($data, $tokens, 'rabies');

    }

    public static function notifyOnReview(ReviewersData $case) {
        $creator = FormData::find($case->case_id)->creator;

        $data = array(
            'case_id' => $case->case_id,
            'category' => 'notif_002',
            'comment' =>  $case->comment,
            'assessment' => $case->assessment
        );

        self::sendPushNotification($data, [$creator->gcm_token]);

    }


    // Payload data you want to send to Android device(s)
    // (it will be accessible via intent extras)

    //$data = array('message' => 'Hello World!');  //--- data come from post_screen_data.php..

    // The recipient registration tokens for this notification
    // https://developer.android.com/google/gcm/

    //$ids = array('abc', 'def');

    // Send push notification via Google Cloud Messaging
    //sendPushNotification($data, $ids );

    public static  function sendPushNotification($data, $ids, $project) {
        // Insert real GCM API key from the Google APIs Console
        // https://code.google.com/apis/console/


        $apiKey = env('FCM_SERVER_KEY_' . strtoupper($project));


        // Set POST request body
        $post = array(
            'registration_ids'  => $ids,
            'data'              => $data,
//            'notification'      => array(
//                'title' => 'Notification test ' . random_int(234,7484),
//                'body' => json_encode($data)
//            )
        );


        // Set CURL request headers
        $headers = array(
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json'
        );

        $response = [];

        // Initialize curl handle
        $ch = curl_init();

        //disable cert verification, u can be hacked...
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // Set URL to GCM push endpoint
        curl_setopt( $ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );

        // Set request method to POST
        curl_setopt( $ch, CURLOPT_POST, true );

        // Set custom request headers
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

        // Get the response back as string instead of printing it
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

        // Set JSON post data
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($post) );

        // Actually send the request
        $result = curl_exec( $ch );

        // Handle errors
        if ( curl_errno( $ch ) ) {
            $response["curl error"] = json_decode(curl_error($ch), true);
        }

        // Close curl handle
        curl_close( $ch );

        // Debug GCM response
        $response["curl result"] =  json_decode($result, true);

        return $response;

    }


}

