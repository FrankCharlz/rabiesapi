<?php

namespace App\Http\Controllers;

use App\Http\Middleware\OAuth2Middleware;
use App\Models\FormData;
use App\Models\ReviewersData;
use App\Models\TreatmentData;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class DataController extends Controller {
    public function __construct() {
        $this->middleware(OAuth2Middleware::class);
    }

    public function getCases($uid) {
        $cases = User::find($uid)->cases;
        return $cases;
    }


    public function addTreatmentData(Request $request) {
        $case = new TreatmentData();
        $case->case_id = $request->get('case_id');
        $case->data = $request->get('data');
        $success = $case->save();

        return array('success' => $success);
    }

    /**
     * @param Request $request - contains form data. no of photos and creators id
     * @return array|string
     */


    public function addFormData(Request $request) {


        $user = User::find($request->get('user_id'));
        $facility = $user->facilities->values()->get(0)->id;

        $caseId = random_int(28437,748373);


       NotificationManager::notifyOnCase($facility, $caseId);

       return $facility;
    }

    /**
     * @param Request $request - case_id, rid, am, comment, assessment
     * @return array - true/false
     */
    public function addReviewData(Request $request) {
        $case = new ReviewersData();
        $case->case_id = $request->get('case_id');
        $case->reviewer_id = $request->get('reviewer_id'); //todo: get from AuthToken
        $case->assessment_mismatch = $request->get('assessment_mismatch', 0);
        $case->data = json_encode(array(
            'assessment' => $request->get('assessment'),
            'comment' => $request->get('comment')
        ));

        $success = $case->save();
        if ($success) NotificationManager::notifyOnReview($case);

        return array('success' => $success);
    }


    /**
     * @param $cid - case id
     * @param $photoId - photo number
     * @return string - path of the photo
     */
    public function renderImage($cid, $photoId) {
        $data = FormData::find($cid)->data;
        $data = json_decode($data, true);

        if (isset($data['photo_'.$photoId])) {
            return $data['photo_'.$photoId];
        } else {
            return '/resource-not-found';
        }
    }

    /**
     * @param $cid - case id
     * @return array - array of images urls for that case
     */
    public function getImages($cid) {
        $data = FormData::find($cid)->data;
        $data = json_decode($data, true);

        $urls= [];
        for ($photoId = 1; $photoId <= 3; $photoId++) {
            if (isset($data['photo_'.$photoId])) $urls[$photoId] = $data['photo_'.$photoId];
        }
        return response()
            ->header("Cache-Control", "private, max-age=86400")
            ->json($urls);
    }

    public function getCaseData($cid) {
        $data = FormData::find($cid)->data;
        return json_decode($data, true);
    }

    /***
     * @param Request $request - userId
     * @return mixed
     *
     * This is called when an ODK instance has been uploaded
     * --> we do not know if it has been successfully saved or no.
     */
    public function instanceUploaded(Request $request) {

        $user = User::find($request->get('userId'));
        $meta = $request->get('instanceId');


        $p = xml_parser_create();
        xml_parse_into_struct($p, $meta, $vals, $index);
        xml_parser_free($p);
        $instanceId = $vals[1]['value'];

        $pos = strrpos($meta, "</meta>");
        if ($pos === false) { // note: three equal signs
            $formId = "";
        } else {
            $formId = substr($meta, $pos + 9);
            $formId = substr($formId, 0, sizeof($formId)  -1);
        }

        DB::table('instances')
            ->insert(['userId' => $user->id, 'uri' => $instanceId, 'formId' => $formId]);


        if ($user->role === 2) {
            return 0;
        }

        $facilities = $user->facilities;

        if ($facilities) {

            $id = $facilities->toArray()[0]['id'];
            NotificationManager::notifyOnCase($id, 0);

        }

        return 0;
    }

}
