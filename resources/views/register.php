<?php
/**
 * Created by PhpStorm.
 * User: Frank
 * Date: 21-Jun-18
 * Time: 10:32
 */

?>

<html>

<?php include 'header.php';?>

<body>

<div class="container">

<!--    <div class="col-md-3"></div>-->
    <div class="col-md-5">

        <form  method="post" action="/register-fac-submit">


            <p class="e-logo">eSurveillance</p>

            <div class="row">
                <div class="form-group-x">
                    <label for="fullname1">Facility name</label>
                    <input type="text"  placeholder="Eg Tarime DDH" id="fullname1"
                           name="fac" autocomplete="name" required>
                </div>

                <div class="form-group-x" id="div-select-fac-single1">
                    <label for="facility1">District</label>
                    <select class="form-control custom-select" id="facility1" name="districtId">
                        <?php
                        foreach($ds as $d) {
                            echo '<option value="'.$d->id.'">'.$d->name.'</option>';
                        }
                        ?>

                    </select>
                </div>

            </div>

            <hr class="above-button">

            <button type="submit" class="btn btn-primary" style="margin: 0  4px 40px;">Add Facility</button>


        </form>
    </div>

    <div class="col-md-7">

        <form  method="post" action="/register-submit">


            <p class="e-logo">eSurveillance</p>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group-x">
                        <label for="fullname">Fullname</label>
                        <input type="text"  placeholder="eg. Dr Maria John" id="fullname"
                               name="fullname" autocomplete="name" required>
                    </div>

                    <div class="form-group-x">
                        <label for="username">Username</label>
                        <input type="text"  placeholder="Username" id="username" name="username" required>
                    </div>


                    <div class="form-group-x">
                        <label for="password">Password</label>
<!--                        todo: password-->
                        <input type="text" placeholder="Password" id="password" name="password" required>
                    </div>


                    <div class="form-group-x">
                        <label for="role">Role</label>
                        <select class="form-control custom-select" id="role" name="role">
                            <option value="1">HW</option>
                            <option value="2">LFO</option>
                        </select>
                    </div>


                </div>
                <div class="col-md-6">
                    <div class="form-group-x">
                        <label for="email">E-mail</label>
                        <input type="text"  placeholder="E-mail" id="email"
                               autocomplete="email" name="email">
                    </div>

                    <div class="form-group-x">
                        <label for="fgi4">Phone number</label>
                        <input type="text"  id="fgi4" placeholder="Phone number"
                               autocomplete="tel" name="phone"  required>
                    </div>


                    <div class="form-group-x" id="div-select-fac-single">
                        <label for="facility">Facility</label>
                        <select class="form-control custom-select" id="facility" name="facility">
                            <?php
                            foreach($facilities as $fac) {
                                echo '<option value="'.$fac->id.'">'.$fac->name.'</option>';
                            }
                            ?>

                        </select>
                    </div>



                </div>
            </div>

            <hr class="above-button">

            <button type="submit" class="btn btn-primary" style="margin: 0 auto 40px;">Add User</button>


        </form>


    </div>


</div>


<style>
    body, html {
        font-family: 'Raleway', sans-serif;
        padding: 0;
        margin: 0;
        background: #e9eceb;
        height: 100%;
    }

    form {
        margin: 20px auto;
        padding: 44px 40px 0;
        background: white;
        display: inline-block;
        border: 1px solid #a8cec2;
        border-radius: 0 0 4px 4px;
        border-top-width: 8px;
        text-align: left;
    }

    form label {
        margin-top: 16px;
        text-align: left;
        font-size: 1em;
        display: block;
    }

    form input {
        display: block;
        margin: 8px -2px;
        border: 0;
        border-bottom: 1px solid  #a8cec2;
        padding: 4px;
        width: 100%;
        letter-spacing: 1px;
        outline: none;


    }

    form  input:hover {
        border-bottom: 1px solid #0b582d
    }

    form  input:active {
        border: 0;
        outline: none;
        border-bottom: 1px solid #0b582d
    }

    button {
        margin: 12px;
        width: 18vw;
        padding: 8px;

    }

    @keyframes logo_animation {
        from {opacity: 0.0;}
        to { opacity: 0.6;}
    }

    img {
        display: block;
        margin: -70px auto 10px auto;
        width: 240px;
        animation-name: logo_animation;
        animation-duration: 2s;
        animation-timing-function: ease-in;
        opacity: 0.6;
    }

    form  span {
        font-size: 2.36em;
        text-align: center;
        display: block;
        margin-bottom: 24px;
    }

    textarea:focus, input:focus{
        outline: none;
    }


    .e-logo {
        font-size: 3.6em;
        font-variant: small-caps;
        font-weight: 400;
        margin: 0;
    }

    .e-logo:first-letter {
        color: #810000;
    }

    .form-group-x {
        padding: 6px 12px;
        max-width: 360px;
    }

    .form-group-x:nth-child(2n) {
        background: #fcfcfe;
    }

    select {
        text-transform: capitalize;
        min-width: 240px;
    }

    .error {
        color: #fa1818;
        text-align: left;
        padding: 12px;
        border-radius: 4px;
        border: 1px solid #fc2121;
        font-weight: 400;
        background: rgba(244, 187, 187, 0.41);
    }

    button {
        font-family: 'open sans', 'raleway', serif;
        background: #0856aa;
    }

    .alert {
        text-align: left;
        font-family: 'open sans', 'raleway', serif;
    }

    .alert h4 {
        font-weight: bold;
    }

    .left-div {
        text-align: left;
        padding: 10% 0 0 12%
    }

    .left-div h1{
        color: #3e3d3d;
    }
    .left-div p {
        font-family: 'Open Sans', sans-serif;
        font-size: 1.10em;
    }

    .right-div {
        text-align: center;
    }

    img {
        display: inline-block;
        margin: -70px auto 10px auto;
        width: 280px;
        animation-name: logo_animation;
        animation-duration: 2s;
        animation-timing-function: ease-in;
        opacity: 0.5;
    }

    .navbar-inverse {
        margin-top: -18px;
        padding: 10px;
    }


    .do-have-account-or {
        display: inline-block;
        vertical-align: bottom;
        font-weight: 600;
        font-family: 'open sans', sans-serif;
    }

    .login-project {
        font-family: 'open sans', sans-serif;
        margin-top: -4px;
        margin-bottom: 12px;
        text-transform: capitalize;
        color: grey;
    }

    .modal-body {
        max-height: 400px;
        overflow-y: scroll;
    }

    #div-select-fac-group, #div-select-fac-multi, #div-select-fac-single0 {
        display: none;
    }

    hr.above-button {
        margin: 28px 0 0;
    }

    .btn-mj {
        margin: 38px 0 0;
    }

    .copyright {
        font-size: 0.7rem;
        text-align: center;
        margin: 20px 0 -2px;
        color: grey;
        display: block;
    }
</style>

</body>
</html>
