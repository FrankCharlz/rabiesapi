<?php
/**
 * Created by PhpStorm.
 * User: Frank
 * Date: 21-Jun-18
 * Time: 10:32
 */

?>

<html>

<?php include 'header.php';?>

<body>

<div class="container-fluid">


    <div class="row">
        <div class="col-md-12 col-xs-12 mb-4">

            <div class="f-details">

                <?php

                function showUser($user) {
                    $roles = ['-', 'HW', 'LFO'];
                    echo  '<div class="user-list-item">';
                    echo '<p> Fullname: <b>' . $user->fullname . ' </b></p>';
                    echo '<p> Name: <b>' . $user->username . ' </b></p>';
                    echo '<p> Phone: <b>'  . $user->phone . ' </b></p>';
                    echo '<p> Password: <b>'  . $user->password . ' </b></p>';
                    echo '<p> Role: <b>' . $roles[(int)$user->role] . ' </b></p>';
                    echo '<p> Description: <b>'  . $user->desc . ' </b></p>';
                    echo '<p><i class="material-icons md-24">edit</i><a href="/users/'.$user->id.'/uaf">Edit</a></p>';
                    echo  '</div>';
                }


                echo '<h3>Facility Details</h3>';
                echo '<p> Id: <b>' . $facility->id . ' </b></p>';
                echo '<p> Name: <b>' . $facility->name . ' </b></p>';
                echo '<p> District: <b>' . $facility->getDistrict->name . '</b></p>';


                $vets = $facility->getDistrict->vets;
                echo '<br><h3>Mifugo</h3>';
                echo  '<p> This district <b><u>('. $facility->getDistrict->name .')  </b></u> has '
                    . sizeof($vets) .' vets';
                if (sizeof($vets) > 0) {
                    foreach ($vets as $user) {
                        showUser($user);
                    }
                }

                echo '<br>';

                echo '<br><h3>Afya</h3>';
                echo  '<p> This facility has '. sizeof($facility->users) .' users';
                if (sizeof($facility->users) > 0) {
                    foreach ($facility->users as $user) {
                       showUser($user);
                    }
                }

                ?>

            </div>

        </div>


    </div>

</div>

<script type="text/javascript">

    $(document).ready(function() {
        console.log('jQuery');

        $(".li-fd").click(function () {

            var uid = $(this).attr("data-user");
            var fid = $(this).attr("data-fac");
            var fdt = $(this).attr("data-fdt");

            console.log(uid, fid);

            $.ajax({
                type: "POST",
                url: "/remove-user-fac",
                data: {
                    userId: uid,
                    facility: fid,
                    type: fdt //0 = fac, 1 = dis
                },
                success: function(result)  {
                    //alert('Succes');
                    window.location.reload(false);
                },
                error: function(data)  {
                    alert('Error: ' +  data.toString());
                }
            });


        });


    });

</script>

<link rel="stylesheet" type="text/css" href="/css/main.css">

</body>
</html>
