<?php
/**
 * Created by PhpStorm.
 * User: Frank
 * Date: 21-Jun-18
 * Time: 10:32
 */

?>

<html>

<?php include 'header.php';?>

<body>

<div class="container-fluid">


    <div class="row">
        <div class="col-md-4 col-xs-12 mb-4">

            <div class="user-details">

                <?php

                $roles = ['-', 'HW', 'LFO'];
                echo '<h3>User Details</h3>';
                echo '<p> Fullname: ' . $user->fullname . '</p>';
                echo '<p> Name: ' . $user->username . '</p>';
                echo '<p> Phone: '  . $user->phone . '</p>';
                echo '<p> Password: '  . $user->password . '</p>';
                echo '<p> Role: ' . $roles[(int)$user->role] . '</p>';
                echo '<p> Description: '  . $user->desc . '</p>';


                if (sizeof($facilities) > 0) {

                    echo '<br><h3>Facilities</h3>';
                    echo '<p class="hint">Hint: Click facility name to un-assign it to a user</p>';
                    echo '<ul>';
                    foreach ($facilities as $fac) {
                        echo '<li class="li-fd" data-user="' . $user->id . '" data-fac="' . $fac->id . '" data-fdt="0">'
                            . $fac->name . ' (#' . $fac->id . ')</li>';
                    }
                    echo '</ul>';
                }

                if (sizeof($districts) > 0) {

                    echo '<br><h3>Districts</h3>';
                    echo '<p class="hint">Hint: Click district name to un-assign it to a user</p>';
                    echo '<ul>';
                    foreach ($districts as $d) {
                        echo '<li class="li-fd" data-user="' . $user->id . '" data-fac="' . $d->id . '" data-fdt="1">'
                            . $d->name . ' (#' . $d->id . ')</li>';
                    }
                    echo '</ul>';
                }

                ?>

            </div>

        </div>

        <div class="col-md-4 col-xs-12 mb-4">

            <form  method="post" action="/assign-fac">

                <p class="e-logo">eSurveillance</p>

                <?php
                echo '<input type="hidden" name="userId" value="'. $user->id.'"> ';
                ?>

                <div class="form-group-x" id="div-select-fac-single">
                    <label for="facility">Facility</label>
                    <select class="form-control custom-select" id="facility" name="facility">
                        <?php
                        foreach($allFacilities as $fac) {
                            echo '<option value="'.$fac->id.'">'.$fac->name.'</option>';
                        }
                        ?>

                    </select>
                </div>

                <hr class="above-button">

                <button type="submit" class="btn btn-primary" style="margin: 0 auto 40px;">
                    <?php
                    echo 'Assign <b> ' . $user->username . '</b> to selected Facility';
                    ?>
                </button>

            </form>


        </div>


        <div class="col-md-4 col-xs-12 mb-4">

            <form  method="post" action="/assign-dis">

                <p class="e-logo">eSurveillance</p>

                <?php
                echo '<input type="hidden" name="userId" value="'. $user->id.'"> ';
                ?>

                <div class="form-group-x" id="div-select-fac-single">
                    <label for="dd">Districts</label>
                    <select class="form-control custom-select" id="dd" name="district">
                        <?php
                        foreach($allDistricts as $dd) {
                            echo '<option value="'.$dd->id.'">'.$dd->name.'</option>';
                        }
                        ?>

                    </select>
                </div>

                <hr class="above-button">

                <button type="submit" class="btn btn-primary" style="margin: 0 auto 40px;">
                    <?php
                    echo 'Assign <b> ' . $user->username . '</b> to selected district';
                    ?>
                </button>

            </form>



        </div>

    </div>

</div>

<script type="text/javascript">

    $(document).ready(function() {
        console.log('jQuery');

        $(".li-fd").click(function () {

            var uid = $(this).attr("data-user");
            var fid = $(this).attr("data-fac");
            var fdt = $(this).attr("data-fdt");

            console.log(uid, fid);

            $.ajax({
                type: "POST",
                url: "/remove-user-fac",
                data: {
                    userId: uid,
                    facility: fid,
                    type: fdt //0 = fac, 1 = dis
                },
                success: function(result)  {
                    //alert('Succes');
                    window.location.reload(false);
                },
                error: function(data)  {
                    alert('Error: ' +  data.toString());
                }
            });


        });


    });

</script>

<link rel="stylesheet" type="text/css" href="/css/main.css">

</body>
</html>
