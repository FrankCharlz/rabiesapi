<?php
/**
 * Created by PhpStorm.
 * User: Frank
 * Date: 21-Jun-18
 * Time: 10:32
 */

?>

<html>

<head>


    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript for bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>


<body>

<div class="container">

    <div class="row">

        <div class="col-md-4">

            <h3>List of Users</h3>
            <table class="table table-hover table-responsive table-condensed">
                <thead>
                <tr>

                    <th>Fullname</th>
                    <th>Role</th>
                    <th>View</th>
                </tr>
                </thead>
                <tbody>

                <?php
                $roles = ['-', 'HW', 'LFO'];
                foreach($users as $user) {
                    echo '<tr>';
                    echo '<td>'.$user->fullname.'</td>';
                    echo '<td>'.$roles[(int)$user->role].'</td>';
                    echo '<td><a href = "/users/'.$user->id.'/uaf">Details/Edit</a></td>';
                    echo '</tr>';
                }
                ?>
                </tbody>

            </table>

        </div>

        <div class="col-md-2"></div>

        <div class="col-md-6">

            <h3>List of Facilities</h3>
            <table class="table table-hover table-responsive table-condensed">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Facility</th>
                    <th>District</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                <?php
                foreach($facilities as $f) {
                    echo '<tr>';
                    echo '<td>'.$f->id.'</td>';
                    echo '<td>'.$f->name.'</td>';
                    echo '<td>'.$f->getDistrict->name.'</td>';
                    echo '<td><a href="/f/view/'.$f->id.'">View</a></td>';
                    echo '</tr>';
                }
                ?>
                </tbody>

            </table>

        </div>

    </div>


</div>


<link rel="stylesheet" type="text/css" href="/css/main.css">

</body>
</html>
