<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\User;
use Illuminate\Support\Facades\DB;


$app->get('/', function () use ($app) {
    return $app->version();
});


$app->post('/login', 'UserController@login');
$app->post('/updateFcmToken', 'UserController@updateFcmToken');

$app->post('/instanceUploaded', 'DataController@instanceUploaded');

$app->get('/user/{id}', function($id) {
    return "Profile ". $id;
});


$app->get('/users/{id}/uaf', function($id) {

    $user = User::find($id);
    $allFacilities = \App\Models\Facility::all();
    $facilities = $user->facilities;
    $districts = $user->districts;
    $allDistricts = \App\Models\District::all();

     return view('uaf')
        ->with('user', $user)
        ->with('allFacilities', $allFacilities)
        ->with('facilities', $facilities)
        ->with('districts', $districts)
        ->with('allDistricts', $allDistricts);
});

$app->get('/f/view/{fid}', function($fid) {
    $facility = \App\Models\Facility::find($fid);
    return view('fdetails')->with('facility', $facility);
});


$app->get('/users/list', function() {
    return view('users')
        ->with('users', User::orderBy('fullname', 'asc')->get())
        ->with('facilities', \App\Models\Facility::all());
});


$app->get('/register', function() {
    $facilities = \App\Models\Facility::orderBy('name', 'asc')->get();
    $districts = \App\Models\District::orderBy('name', 'asc')->get();

    return view('register')
        ->with('facilities', $facilities)
        ->with('ds', $districts);


});

$app->post('/register-submit', 'UserController@registerUser');
$app->post('/assign-fac', 'UserController@assignFac');
$app->post('/assign-dis', 'UserController@assignDis');

$app->post('/register-fac-submit', 'UserController@registerFacility');


$app->post('/remove-user-fac', 'UserController@removeUserFac');
$app->post('/remove-user-dis', 'UserController@removeUserDis');


$app->get('/users/{uid}/cases', 'DataController@getCases');
$app->post('/users/{uid}/update/token/{token}', 'UserController@updateToken');
$app->post('/user/update-token', 'UserController@updateToken2');
$app->post('/users/update-token', 'UserController@updateToken2');
$app->get('/provider/{uid}/cases/reviewed', 'DataController@getReviewedCases');
$app->get('/reviewer/{uid}/cases/un-reviewed', 'DataController@getUnReviewedCases');


$app->get('/cases/{cid}', 'DataController@getCaseData');
$app->get('/cases/{cid}/images', 'DataController@getImages');
$app->get('/render-image/{cid}/{photoId}', 'DataController@renderImage');
$app->get('/render-image-compat', 'ImageUtils@renderImageCompat');
$app->get('/render-thumb-compat', 'ImageUtils@renderThumbCompat');


$app->post('/cases/add', 'DataController@addFormData');



$app->post('/cases/review/add', 'DataController@addReviewData');
$app->post('/cases/treatment/add', 'DataController@addTreatmentData');
$app->post('/cases/mark-resolved', 'DataController@markResolved');

$app->get('/cases/reviewed/{cid}', 'DataController@getReviewedCaseData');

$app->get('/cases/mismatched/provider/{pid}', 'DataController@getMismatchedAssessmentsForProvider');
$app->get('/cases/mismatched/reviewer/{rid}', 'DataController@getMismatchedAssessmentsForReviewer');


$app->get('/facility-names-and-codes', function() {

    $contents = file_get_contents('http://cervical.esurveillance.or.tz/v3/_facility_data_raw');
    return  array(
        'success' => true,
        'data' => $contents

    );
});


$app->get('/versions', function() {

    $data = DB::table('app_data')->where('key', 'CONFIG')->get()[0]->value;

    $data = json_decode($data, true);

    return $data;

});
